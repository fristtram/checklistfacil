<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterestInTheCakes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_in_the_cakes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('interested_id');
            $table->unsignedBigInteger('cake_id');

            $table->boolean('send_email')->default(false);
            $table->timestamps();

            $table->foreign('interested_id')->references('id')->on('interesteds');
            $table->foreign('cake_id')->references('id')->on('cakes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_in_the_cakes');
    }
}
