<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cake extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weight_id',
        'name',
        'price',
        'amount'
    ];

    public function weight()
    {
        return $this->belongsTo(Weight::class);
    }
}
