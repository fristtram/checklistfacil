<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestInTheCake extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'interested_id',
        'cake_id',
        'send_email'
    ];


    public function cake()
    {
        return $this->belongsTo(Cake::class);
    }

    public function interested()
    {
        return $this->belongsTo(Interested::class);
    }
}
