<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\InterestInTheCake;
use App\Models\Interested;
use App\Models\Cake;
use App\Jobs\EmailInterestInCake;

class InterestInCakeController extends Controller
{
    protected $interestInTheCake;
    protected $interested;
    protected $cake;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(
        InterestInTheCake $interestInTheCake,
        Interested $interested,
        Cake $cake
        )
    {
        $this->interestInTheCake = $interestInTheCake;
        $this->interested = $interested;
        $this->cake = $cake;
    }

    public function createInterested(Request $reques)
    {
        DB::beginTransaction();
        try {
            $data = $reques->all();

            // Validação dos campo
            $validation = Validator::make($data, [
                'email' => 'required|email',
                'cake_id' => 'required'
            ]);
            if ($validation->fails()) {
                $erro = $validation->errors();
                $message = '';
                for ($i=0; $i < count($erro) ; $i++) {
                    $result = json_decode($erro);
                    if (isset($result->email[$i])) {
                       $message = $result->email[$i];
                    }
                    if (isset($result->cake_id[$i])) {
                       $message = $result->cake_id[$i];
                    }
                }

                return response()->json([
                    'success' => false,
                    'message' => $message,
                    'result' => false
                ], 403);
            }

            // Criar o interessado
            $in_list = $this->interested::where('email', $data['email'])->first();
            if (!$in_list) {
                $in_list = $this->interested::create([
                    'email' => $data['email']
                ]);
            }

            // Colocar um processo de interesse na fila
            $interested = $this->interestInTheCake::create([
                'cake_id' => $data['cake_id'],
                'interested_id' => $in_list->id
            ]);
            DB::commit();

            // Chamar o job
            EmailInterestInCake::dispatch($interested)->delay(now()->addSeconds('15'));

            return response()->json([
                'success' => true,
                'message' => 'Interested successfully created!',
                'result' => $interested
            ], 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
