<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Jobs\EmailInterestInCake;
use App\Models\Cake;
use App\Models\Weight;

class CakeController extends Controller
{
    protected $cake;
    protected $weight;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(Cake $cake, Weight $weight)
    {
        $this->cake = $cake;
        $this->weight = $weight;
    }

    public function createWeight(Request $reques)
    {
        DB::beginTransaction();
        try {
            $data = $reques->all();

            // Validação dos campo
            $validation = Validator::make($data, [
                'name' => 'required'
            ]);
            if ($validation->fails()) {
                $erro = $validation->errors();
                $message = '';
                for ($i=0; $i < count($erro) ; $i++) {
                    $result = json_decode($erro);
                    if (isset($result->name[$i])) {
                       $message = $result->name[$i];
                    }
                }

                return response()->json([
                    'success' => false,
                    'message' => $message,
                    'result' => false
                ], 403);
            }

            // Criar tipo da medida de peso do bolo
            $result = $this->weight::create([
                'name' => $data['name']
            ]);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Successfully created weight!',
                'result' => $result
            ], 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function getWeight()
    {
        // Buscar os tipo da medida do peso
        $result = $this->weight::get();

        return response()->json([
            'success' => true,
            'message' => 'Weight listed successfully',
            'result' => $result
        ], 201);
    }

    public function createCake(Request $reques)
    {
        DB::beginTransaction();
        try {
            $data = $reques->all();

            // Validação dos campo
            $validation = Validator::make($data, [
                'weight_id' => 'required',
                'name' => 'required',
                'price' => 'required',
                'amount' => 'required|integer'
            ]);
            if ($validation->fails()) {
                $erro = $validation->errors();
                $message = '';
                for ($i=0; $i < count($erro) ; $i++) {
                    $result = json_decode($erro);
                    if (isset($result->weight_id[$i])) {
                       $message = $result->weight_id[$i];
                    }
                    if (isset($result->name[$i])) {
                       $message = $result->name[$i];
                    }
                    if (isset($result->price[$i])) {
                       $message = $result->price[$i];
                    }
                    if (isset($result->amount[$i])) {
                       $message = $result->amount[$i];
                    }
                }

                return response()->json([
                    'success' => false,
                    'message' => $message,
                    'result' => false
                ], 403);
            }

            // Criar bolo
            $result = $this->cake::create([
                'weight_id' => $data['weight_id'],
                'name' => $data['name'],
                'price' => $data['price'],
                'amount' => $data['amount']
            ]);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Successfully created cake!',
                'result' => $result
            ], 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Error creating cake. Error: '.$e->getMessage()
            ], 400);
        }
    }

    public function getCake()
    {
        // Buscar uma lista de bolos
        $result = $this->cake::get();

        return response()->json([
            'success' => true,
            'message' => 'Cake listed successfully',
            'result' => $result
        ], 201);
    }

    public function addAmountCake(Request $reques)
    {
        DB::beginTransaction();
        try {
            $data = $reques->all();

            // Validação dos campo
            $validation = Validator::make($data, [
                'amount' => 'required|integer'
            ]);
            if ($validation->fails()) {
                $erro = $validation->errors();
                $message = '';
                for ($i=0; $i < count($erro) ; $i++) {
                    $result = json_decode($erro);
                    if (isset($result->amount[$i])) {
                       $message = $result->amount[$i];
                    }
                }
                return response()->json([
                    'success' => false,
                    'message' => $message,
                    'result' => false
                ], 403);
            }

            // Atualizar a quantidade de bolo
            $cake = $this->cake::find($data['id']);
            $cake->update(['amount' => $data['amount']+$cake->amount]);

            DB::commit();

            // Chamar o job
            EmailInterestInCake::dispatch($interested)->delay(now()->addSeconds('15'));

            return response()->json([
                'success' => true,
                'message' => 'Amount added successfully!',
                'result' => $cake
            ], 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Error adding amount. Error: '.$e->getMessage()
            ], 400);
        }
    }
}
