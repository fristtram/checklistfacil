<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\InterestInTheCake;
use App\Models\Cake;
use Mail;

class EmailInterestInCake implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $interestInTheCake;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(InterestInTheCake $interestInTheCake)
    {
        $this->interestInTheCake = $interestInTheCake;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Buscar um pedido pendente para enviar o e-mail
        $interest = $this->interestInTheCake::select('interesteds.email', 'cakes.id as id_cake', 'interest_in_the_cakes.id')
            ->join('cakes', 'cakes.id', '=', 'interest_in_the_cakes.cake_id')
            ->join('interesteds', 'interesteds.id', '=', 'interest_in_the_cakes.interested_id')
            ->where('cakes.amount', '>', 0)
            ->where('interest_in_the_cakes.send_email', '=', 0)
            ->first();

        if ($interest) {
            // Simulando a saido do bolo para cada envio de e-mail
            $cake = Cake::find($interest->id_cake);
            $cake->update(['amount' => $cake->amount - 1]);

            // Confirmar a chamado do envio de e-mail
            $interestInTheCake = $this->interestInTheCake::find($interest->id);
            $interestInTheCake->update(['send_email' => 1]);
            $emailDestino = $interest->email;

            /**
             * Envio de e-mail
             */
            Mail::send(
                'email.info',
                [],
                function($message) use ($emailDestino){
                    $message->to($emailDestino);
                    $message->subject("Aviso da disponibilidade de bolo");
                }
            );
        }

    }
}
