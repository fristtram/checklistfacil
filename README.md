## Gerar arquivo .env a partir do Example
cp .env.example .env
php artisan key:generate

## Instalar os pacotes
composer install

## Criar Banco de Dados
Database: confeitaria_checklist

## Criar tabelas na base de dados
php artisan migrate:install
php artisan migrate

## Rodar Jobs para desparo dos e-mail
php artisan queue:work database --daemon --tries=5




