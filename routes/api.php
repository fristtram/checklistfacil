<?php
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portugues');
date_default_timezone_set('America/Sao_Paulo');

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CakeController;
use App\Http\Controllers\InterestInCakeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/v1')->group( function() {

    Route::prefix('/weight')->group( function() {
        Route::post('/create', [CakeController::class, 'createWeight']);
        Route::get('/list', [CakeController::class, 'getWeight']);
    });
    Route::prefix('/cake')->group( function() {
        Route::post('/create', [CakeController::class, 'createCake']);
        Route::get('/list', [CakeController::class, 'getCake']);
        Route::put('/add-amount', [CakeController::class, 'addAmountCake']);
    });
    Route::prefix('/interested')->group( function() {
        Route::post('/create', [InterestInCakeController::class, 'createInterested']);
        Route::get('/notice', [InterestInCakeController::class, 'availabilityNotice']);
    });
});
